## Unit Test Sample Project
### Getting Started
To begin working with this sample on unit testing:
1. Download the files from the master branch, 
then upload them to the main branch of a repository of your own making.
2. Create a feature branch from the main branch you created in step 1.
3. Add a new project to the solution using the xUnit C# Template
4. Add the following nuget package
    * Microsoft.AspNetCore.Mvc.Core
    * Moq
5. Add a reference to the UnitTestingTraining.Api and UnitTestingTraining.Data
projects in the new testing project

### Training Challange
You must complete the following tasks to complete the training challange:
1. Add a new xUnit Test class called PersonControllerTest to the test project
2. Add a GetNoMiddleNameTest Fact unit test method and test the Get operation 
that returns a list with a single user that has no middle name
3. Add a GetWithMiddleNameTest Fact unit test method and test the Get operation 
that returns a list with a single user that has a middle name
4. Add a GetTest Theory unit test method and test the Get operation 
that returns a list various combinations of values on a single user in the list
5. Add a GetByIdTest Theory unit test method and test the Get operation 
that returns a single record based on an id using various combinations of values for the fields

If you get stuck, you can take a peak at a fully implemented solution in this repo under the
feat/full-solution feature branch.