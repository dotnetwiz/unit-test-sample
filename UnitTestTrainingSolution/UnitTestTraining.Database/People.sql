﻿CREATE TABLE [dbo].[People] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]   NVARCHAR (50)  NOT NULL,
    [MiddleName]  NVARCHAR (50)  NULL,
    [LastName]    NVARCHAR (100) NOT NULL,
    [DateOfBirth] DATE           NULL,
    [Phone]       NVARCHAR (50)  NULL,
    [Email]       NVARCHAR (256) NULL,
    CONSTRAINT [PK__People] PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO

CREATE INDEX [IX_People_Names] ON [dbo].[People] ([FirstName], [MiddleName], [LastName])
