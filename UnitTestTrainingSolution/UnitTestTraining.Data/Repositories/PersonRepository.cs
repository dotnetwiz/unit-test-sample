﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTestTraining.Data.Models;

namespace UnitTestTraining.Data.Repositories
{
    /// <summary>
    /// Repository class for working with the Person entity.
    /// </summary>
    public class PersonRepository : RepositoryBase<Person, int>
    {
        //Initializes a new instance of the PersonRepository class.
        public PersonRepository(ContactsContext context) : base(context) { }
    }
}
