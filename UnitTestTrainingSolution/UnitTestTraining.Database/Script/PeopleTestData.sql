﻿
INSERT INTO [dbo].[People]
([FirstName], [MiddleName], [LastName], [DateOfBirth],       [Phone],                                 [Email])
VALUES
(     'John',      'Henry',    'Smith',	   '1/1/1997', '15555553555',      'john.smith@nonexistentdomain.dne'),
(     'Jane',  'Henrietta',   'Camdon',	 '12/15/1980', '15555555545',   'jane.h.camdon@nonexistentdomain.dne'),
( 'Fredrick',         NULL,   'Richie',	  '6/14/1990', '15555883598',        'f.richie@nonexistentdomain.dne'),
(    'Wayne',      'Drake',     'Hahn',	 '10/25/1988', '15557750515',          'wdhahn@nonexistentdomain.dne')
