﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestTraining.Data.Models
{
    /// <summary>
    /// Model class representing a person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the identifier of the person.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the first name of the person.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name of the person.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name (surname) of the person.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the date of birth of the person.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the phone number for the person.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the email address for the person.
        /// </summary>
        public string Email { get; set; }
    }
}
